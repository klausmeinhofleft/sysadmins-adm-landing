R'lyeh Hacklab Sysadmin Commitment

At R'lyeh Hacklab we take security and privacy very seriously and we do all in our power to protect our users.

I, a R'lyeh Hacklab sysadmin, fully commit to:

* Never read our users' data.
* Protect our users' data at all costs, preferring losing it than disclosing it.
* Adopt and follow good security practices.
* Be transparent with our actions and methods.
* Promote social ownership and democratic control over information, ideas, technology, and the means of communication.
* Rely on open source and open hardware whenever possible.
* Responsibly notify in any possible way if our users' data is compromised.

For which I sign this commitment.

